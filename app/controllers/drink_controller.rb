class DrinkController < ApplicationController
    
    def index
        @drinks = Drink.all 
    end

    def show
        @drink = Drink.find(params[:id])
    end

    def new
        @drink = Drink.new(drink_params)

        if @drink.soda 
            @drink.price = 3.00
        elsif @drink.juice && @drink.small
            @drink.price = 1.00
        elsif @drink.juice && @drink.large
            @drink.price = 2.50
        else
            @drink.price = 2.00
        end
    end

    def drink_new_params
        @drink = Drink.new(drink_params)

        if @drink.soda 
            @drink.price = 3.00
        elsif @drink.juice && @drink.small
            @drink.price = 1.00
        elsif @drink.juice && @drink.large
            @drink.price = 2.50
        else
            @drink.price = 2.00
        end
    end
  
    def edit
        @drink = Drink.find(params[:id])        
    end
  
    def create
        
        @drink = Drink.new(drink_params)        
        @drink.order_id = current_user.orders.last.id
        

        if @drink.soda 

            @drink.price = 3.00
            @drink.juice = false
            @drink.small = false
            @drink.large = false

        elsif @drink.juice && @drink.small
            
            @drink.price = 1.00
            @drink.soda  = false
            @drink.large = false

        elsif @drink.juice && @drink.large
            
            @drink.price = 2.50
            @drink.soda  = false
            @drink.small = false

        elsif @drink.juice && @drink.small==nil && @drink.large==nil
            
            @drink.price = 1.00
            @drink.soda  = false
            @drink.small = true
            @drink.large = false

        elsif @drink.soda==nil && @drink.juice==nil && @drink.small==nil && drink.large==nil

            @drink.price = 0.0
            @drink.soda  = false
            @drink.juice = false
            @drink.small = false
            @drink.large = false

        end


        @drink.save
    end
  
    def update
        @drink = Drink.update(drink_params)
        @drink.save
    end
  
    def destroy
        Drink.find(params[:id]).destroy
    end

    private 
        def drink_params
            params.require(:drinks).permit(:soda, :juice, :small, :large)
        end

end
