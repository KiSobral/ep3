class LunchBoxController < ApplicationController

    def index
        @lunch_boxes = LunchBox.all
    end

    def show
        @lunch_box = LunchBox.find(params[:id])
    end

    def new
        @lunch_box = LunchBox.new(lunch_box_params)
        @lunch_box.price = 8.00
    end
  
    def edit
        @lunch_box = LunchBox.find(params[:id])
    end
  
    def create
        
        @lunch_box = LunchBox.new(lunch_box_params)
        @lunch_box.order_id = current_user.orders.last.id
        @lunch_box.price = 8.00
        
        if @lunch_box.rice==nil
            @lunch_box.rice = false
        end

        if @lunch_box.meat==nil
            @lunch_box.meat = false
        end

        if @lunch_box.flour==nil
            @lunch_box.flour = false
        end

        if @lunch_box.bean==nil
            @lunch_box.bean = false
        end

        if @lunch_box.manioc==nil
            @lunch_box.manioc = false
        end

        if @lunch_box.potato==nil
            @lunch_box.potato = false
        end

        if @lunch_box.vinaigrette==nil
            @lunch_box.vinaigrette = false
        end

        if @lunch_box.rice!=nil || @lunch_box.meat!=nil || @lunch_box.flour!=nil || @lunch_box.bean!=nil || @lunch_box.manioc!=nil || @lunch_box.potato!=nil || @lunch_box.vinaigrette!=nil 
            @lunch_box.save
            
        end

    end
  
    def update
        @lunch_box = LunchBox.update(lunch_box_params)
        @lunch_box.save
    end
  
    def destroy
        LunchBox.find(params[:id]).destroy
    end

    private
        def lunch_box_params
            params.require(:lunch_boxes).permit(:rice, :meat, :flour, :bean, :manioc, :potato, :vinaigrette)
        end

end
