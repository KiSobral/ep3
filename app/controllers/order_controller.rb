class OrderController < ApplicationController

  def index
    @all_orders = Order.all

    @last_order = Order.last.price

    @all_orders.each do |order|
    
      if order.created_at.day==DateTime.now.day && order.created_at.month==DateTime.now.month && order.created_at.year==DateTime.now.year
        @orders << order
      end
    
    end
  
  end

  def show
    @order = Order.find(params[:id])
  end

  def new
    @order = current_user.orders.build
  end

  def edit
    @order = Order.find(params[:id])
  end

  def create  
    # Saving order in db
    @order = current_user.orders.build
    @order.delivered = false
    @order.save
    

    # Saving lunch_boxes in db
    @lunch_box = LunchBox.new(lunch_box_params)
    @lunch_box.order_id = current_user.orders.last.id
    @lunch_box.price = 8.00
    
    if @lunch_box.rice==nil
        @lunch_box.rice = false
    end

    if @lunch_box.meat==nil
        @lunch_box.meat = false
    end

    if @lunch_box.flour==nil
        @lunch_box.flour = false
    end

    if @lunch_box.bean==nil
        @lunch_box.bean = false
    end

    if @lunch_box.manioc==nil
        @lunch_box.manioc = false
    end

    if @lunch_box.potato==nil
        @lunch_box.potato = false
    end

    if @lunch_box.vinaigrette==nil
        @lunch_box.vinaigrette = false
    end

    if @lunch_box.rice!=nil || @lunch_box.meat!=nil || @lunch_box.flour!=nil || @lunch_box.bean!=nil || @lunch_box.manioc!=nil || @lunch_box.potato!=nil || @lunch_box.vinaigrette!=nil 
      @lunch_box.save
    end


    # Saving drinks in db
    @drink = Drink.new(drink_params)        
    @drink.order_id = current_user.orders.last.id
    if @drink.soda 

      @drink.price = 3.00
      @drink.juice = false
      @drink.small = false
      @drink.large = false

    elsif @drink.juice && @drink.small
            
      @drink.price = 1.00
      @drink.soda  = false
      @drink.large = false

    elsif @drink.juice && @drink.large
            
      @drink.price = 2.50
      @drink.soda  = false
      @drink.small = false

    elsif @drink.juice && @drink.small==nil && @drink.large==nil
            
      @drink.price = 1.00
      @drink.soda  = false
      @drink.small = true
      @drink.large = false

    elsif @drink.soda==nil && @drink.juice==nil && @drink.small==nil && drink.large==nil

      @drink.price = 0.0
      @drink.soda  = false
      @drink.juice = false
      @drink.small = false
      @drink.large = false

    end
    @drink.save


    # updating order price
    @order.price = @lunch_box.price + @drink.price
    @order.save

  end

  def update
    @order = Order.update(params[:id], order_param)
    @order.save
  end

  def destroy
    @order = Order.find(params[:id]).destroy    
  end
    
  private
    def order_param
      params.require(:orders).permit(:delivered)
    end
    def lunch_box_params
      params.require(:orders).permit(:rice, :meat, :flour, :bean, :manioc, :potato, :vinaigrette)
    end

    def drink_params
      params.require(:orders).permit(:soda, :juice, :small, :large)
    end

end
