class Order < ApplicationRecord
    # 'has_one' association between classes
    has_one :lunch_box
    has_one :drink

    # 'belongs_to' association between classes
    belongs_to :user
end
