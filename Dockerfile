FROM ruby:2.5.1
RUN apt-get update -qq && apt-get install -y nodejs postgresql-client
RUN mkdir /ep3
WORKDIR /ep3
COPY Gemfile /ep3/Gemfile
COPY Gemfile.lock /ep3/Gemfile.lock
RUN gem install pg execjs
ENV BUNDLER_VERSION 2.0.2
RUN gem install bundler && bundle install
COPY . /ep3

# Add a script to be executed every time the container starts.:
EXPOSE 3000

# Start the main process.
CMD ["rails", "server", "-b", "0.0.0.0"]


