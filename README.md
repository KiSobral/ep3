![](/app/assets/images/background_read_me.png)

# FAST GREGO

O site de pedidos de marmita FastGrego é uma aplicação web, isto é, um software desenvolvido para ser utilizado através de um navegador de internet. O FastGrego foi desenvolvido por Hugo Sobral de Lima Salomão e Leonardo da Silva Gomes, alunos de Engenharia de Software da Universidade de Brasília.
O site foi desenvolvido com a futura aspiração de ser transformado em um projeto real e funcional. O projeto tem como cliente o comerciante local conhecido como "Grego". A ideia do site foi apresentada e aprovada pelo mesmo.


## Objetivos

Após a reunião para saber as expectativas do comerciante em relação ao site, foram levantados dois grandes objetivos para a aplicação FastGrego, Estas são: fornecer melhor acessibilidade e facilidade aos clientes que desejam realizar pedidos de marmitas, e também, proporcionar aos funcionários do comércio uma maior facilidade do controle de pedidos.
    
A facilidade e acessibilidade de pedidos é proveniente da página inicial de visualização dos clientes. Nesta página, é possível escolher todos os ingredientes que irão compor a marmita de determinado pedido. De forma análoga, as bebidas possuem suas características explícitas no cardápio e o cliente pode escolher sua bebida em apenas alguns cliques.

Para os funcionários, o gerenciamento de pedidos é facilitado pela identificação do número do pedido e nome do usuário responsável pelo pedido. Para o funcionário, a página mostra os pedidos inseridos em um display vermelho que se separa em duas listas, uma responsável por mostrar os pedidos que foram feitos e ainda não foram entregues aos clientes. Um funcionário pode mover um pedido da lista de “não entregues” para a lista de “entregues”. 

![](/app/assets/images/right_page.gif)


## Identidade Visual 

A Faculdade do Gama da Universidade de Brasília possui uma disciplina focada na interação entre humanos e computadores. Durante o primeiro semestre do ano de 2019, um grupo de alunos que cursou a disciplina estava desenvolvendo uma aplicação voltada para o comércio local do "Grego".
Desta forma, viabilizamos um canal de comunicação com o grupo composto pelos alunos Alan Lima, Arthur Rodrigues, Lucas Machado e Marco Antônio
.

Após as trocas de informação, tanto a identidade visual quanto o nome da aplicação foram definidas. Em meio a conversas com o grupo e análise de protótipos, surge a aplicação web "FastGrego", com a identidade visual baseada no aplicativo mobile desenvolvido para a disciplina. A paleta de cores escolhida teve como inspiração os molhos usados pelo comércio, desta forma, o site foi desenhado para lembrar o visual dos condimentos.


![](/app/assets/images/order_page.gif)

## Dockerização

Por se tratar de uma aplicação com o futuro objetivo de ser executada em contextos reais, era imprescindível que o ambiente da aplicação estivesse isolado. Com isto em mente, o Docker surgiu como uma opção viável devido a possibilidade de isolar o ambiente em camadas menos complexas de containers e definir configurações padrão para as futuras contribuições e manutenção do código.


## Comportamento de execução

Com todos os planejamentos prontos, a fase de desenvolvimento ficou pronta para ser rodada. A dupla organizou o repositório do GitLab em branches distintas para cada funcionalidade da aplicação, desta forma, pode-se trabalhar com organização e lucidez de projeto.

Uma metodologia bastante adotada durante o desenvolvimento foi o Dojo, na qual um integrante do grupo é responsável por escrever códigos de programação enquanto o outro realiza as pesquisas com o intuito de sanar eventuais dúvidas e direcionar o progresso. Dentro da metodologia, o indivíduo que está programando é chamado de "piloto" enquanto o indivíduo que está fornecendo auxílio é chamado de "copiloto".
