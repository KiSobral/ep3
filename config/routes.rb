Rails.application.routes.draw do
  get 'home/home'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  devise_for :users

  # Add paths to Orders, Lunch_Boxes and Drinks
  resources :order
  resources :lunch_box
  resources :drink

  # Root to homepage
  root 'home#home'
end
