class CreateDrinks < ActiveRecord::Migration[5.2]
  def change
    create_table :drinks do |t|
      t.boolean :soda
      t.boolean :juice
      t.boolean :small
      t.boolean :large
      t.decimal :price

      t.timestamps
    end
  end
end
