class CreateLunchBoxes < ActiveRecord::Migration[5.2]
  def change
    create_table :lunch_boxes do |t|
      t.boolean :rice
      t.boolean :meat
      t.boolean :bean
      t.boolean :flour
      t.boolean :manioc
      t.boolean :potato
      t.boolean :vinaigrette
      t.decimal :price

      t.timestamps
    end
  end
end
