class AddOrderRefToLunchBoxes < ActiveRecord::Migration[5.2]
  def change
    add_reference :lunch_boxes, :order, foreign_key: true
  end
end
