class AddOrderRefToDrinks < ActiveRecord::Migration[5.2]
  def change
    add_reference :drinks, :order, foreign_key: true
  end
end
