class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.decimal :price
      t.boolean :delivered

      t.timestamps
    end
  end
end
